#! /bin/sh

echo "Waiting for Postgres"

# Reference the Postgres container until connection is successful
while ! nc -z api-db 5432; do
  sleep 0.10
done

echo "PostgreSQL started"

python manage.py run -h 0.0.0.0
