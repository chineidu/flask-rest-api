# Pull official base image
FROM python:3.10.3-slim-buster

# Set working directory
WORKDIR /usr/src/app

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install system dependencies
RUN apt-get update \
    && apt-get -y install netcat gcc postgresql \
    && apt-get clean

# Add and install requirements
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# Add app
COPY . .

# Add entrypoint
COPY ./entrypoint.sh .
RUN chmod +x /usr/src/app/entrypoint.sh 
