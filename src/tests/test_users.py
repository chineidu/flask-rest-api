# Built-in modules
from http import client
import json

# Custom imports
from src import db
from src.api.models import User
from src.tests.conftest import add_user


def test_add_user(test_app, test_database):
    # Given
    client = test_app.test_client()

    # When: Add data
    resp = client.post(
        "/users",
        data=json.dumps({"username": "michael", "email": "michael@testdriven.io"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())

    # Then
    assert resp.status_code == 201
    assert "michael@testdriven.io was added" in data.get("message")


def test_add_user_invalid_json(test_app, test_database):
    # Given
    client = test_app.test_client()

    # When: Add data
    resp = client.post(
        "/users",
        data=json.dumps({}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())

    # Then
    assert resp.status_code == 400
    assert "Input payload validation failed" in data.get("message")


def test_add_user_invalid_json_keys(test_app, test_database):
    # Given
    client = test_app.test_client()

    # When: Add data
    resp = client.post(
        "/users",
        data=json.dumps({"email": "jphn@testdriven.io"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())

    # Then
    assert resp.status_code == 400
    assert "Input payload validation failed" in data.get("message")


def test_add_user_duplicate_email(test_app, test_database):
    # Given
    client = test_app.test_client()

    # When
    client.post(
        "/users",
        data=json.dumps(
            {
                "username": "michael",
                "email": "michael@testdriven.io",
            },
        ),
        content_type="application/json",
    )
    # Add the same data again
    resp = client.post(
        "/users",
        data=json.dumps(
            {
                "username": "michael",
                "email": "michael@testdriven.io",
            },
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())

    # Then
    assert resp.status_code == 400
    assert "Sorry. That email already exists." in data.get("message")


def test_single_user(test_app, test_database, add_user):
    # Given
    client = test_app.test_client()

    # When
    user = add_user("jeffrey", "jeffrey@testdriven.io")
    resp = client.get(f"/users/{user.id}")
    data = json.loads(resp.data.decode())

    # Then
    assert resp.status_code == 200
    assert "jeffrey" in data.get("username")
    assert "jeffrey@testdriven.io" in data.get("email")


def test_single_user_incorrect_id(test_app, test_database):
    # Given
    client = test_app.test_client()

    # When
    resp = client.get("/users/999")
    data = json.loads(resp.data.decode())

    # Then
    assert "User 999 does not exist" in data.get("message")


def test_all_users(test_app, test_database, add_user):
    # Delete all the users in the db
    test_database.session.query(User).delete()

    # When: Add users
    add_user("michael", "michael@mherman.org")
    add_user("fletcher", "fletcher@notreal.org")

    # When
    client = test_app.test_client()
    resp = client.get("/users")
    data = json.loads(resp.data.decode())

    # Then
    assert resp.status_code == 200
    assert len(data) == 2
    assert "michael" in data[0].get("username")
    assert "michael@mherman.org" in data[0].get("email")
    assert "fletcher" in data[1].get("username")
    assert "fletcher@notreal.org" in data[1].get("email")
