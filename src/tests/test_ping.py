import json


def test_ping(test_app):
    # Given: i.e the state of the app b4 the test runs
    client = test_app.test_client()

    # When: i.e the behavior/logic being tested
    resp = client.get("/ping")
    data = json.loads(resp.data.decode())

    # Then: i.e the expected changes based on the behavior
    assert resp.status_code == 200
    assert "pong" in data.get("message")
    assert "success" in data.get("status")
