# 3rd party libraries
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# Built-in libraries
import os


# Instantiate the db
db = SQLAlchemy()


def create_app(script_info=None):
    # Instantiate the app
    app = Flask(__name__)

    # Set config
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)

    # Set up extensions
    db.init_app(app)

    # Register blueprints
    from src.api.ping import ping_blueprint
    from src.api.users import users_blueprint

    app.register_blueprint(ping_blueprint)
    app.register_blueprint(users_blueprint)

    # Shell context for Flask CLI
    @app.shell_context_processor
    def ctx():
        """This is used to register the app and the db to
         the the shell. i.e you can use the app and db without
         importing it to the shell."""

        return {"app": app, "db": db}

    return app
