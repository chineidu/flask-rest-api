# 3rd party libraries
from flask.cli import FlaskGroup

# Built-in libraries
import sys

# Custom imports
from src import create_app, db
from src.api.models import User

app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command("recreate_db")
def recreate_db():
    """This registers a new command 'recreate_db' to the CLI so that
    it can be used from the command line.
    """
    db.drop_all()
    db.create_all()
    db.session.commit()


if __name__ == "__main__":
    cli()


### Notes:
# 1. Create a db connection
# docker-compose exec api python manage.py recreate_db
# 2.
